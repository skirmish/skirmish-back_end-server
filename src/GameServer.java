import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;

import exceptions.EndGameException;
import exceptions.PacketReceiveException;
import exceptions.PortUnavailableException;

public class GameServer{
	/* SOCKET PROGRAMMING ATTRIBUTES */
	private static final int MAXIMUM_BUFFER_SIZE = 1024;
	private final DatagramSocket serverSocket;
	private volatile boolean alive;
	private Thread clientListener;
	private final Map<String,NetworkPlayer> netClients;
	private GameStatus gameStatus;
	
	/* GUI ATTRIBUTES*/
	private final ServerUI serverUI;
	
	public GameServer(int port){
		serverUI = new ServerUI();
		try {
			serverSocket = new DatagramSocket(port);
			netClients = new HashMap<String,NetworkPlayer>();
			gameStatus = GameStatus.WAITING;
			setupClientListener();
			startClientListener();
			display();
			logStatus("Server started on port "+port);
		} catch (SocketException e) {
			throw new PortUnavailableException("Cannot listen to port "+port+".");
		}
	}
	
	private void display() {
		serverUI.display();
		
	}
	
	//send data to user
	public void sendToUser(String username, String message){
		if(netClients.containsKey(username)){
			NetworkPlayer n = netClients.get(username);
			InetAddress address = n.getAddress();
			int port = n.getPort();
			send(address, port, message);
		}else{
			
		}
		
	}

	//send data to clients
	public void broadcast(String message){
		for(String key : netClients.keySet()){
			InetAddress address = netClients.get(key).getAddress();
			int port = netClients.get(key).getPort();
			send(address,port,message);
		}
	}
	
	//send data to client
	public void send(InetAddress address, int port, String message){
		byte[] messageBuff = message.getBytes();
		DatagramPacket packet = new DatagramPacket(messageBuff, 
									messageBuff.length,
									address, port);
		try {
			serverSocket.send(packet);
			logSentPacket(address.toString(),message);
		} catch (IOException e) {
			logStatus("Error sending packet: "+message);
		}
	}
	
	private void setupClientListener(){
		clientListener = new Thread(new Runnable(){
			@Override
			public void run() {
				alive = true;
				while(alive){
					byte[] buff = new byte[MAXIMUM_BUFFER_SIZE];
					DatagramPacket packet = new DatagramPacket(buff,buff.length);
					
					try {
						serverSocket.receive(packet);
						String data = new String(buff).trim();
						
						if(data.startsWith("CONNECTION_REQUEST")){
							String username = data.split("\\s")[1];
							InetAddress address = packet.getAddress();
							int port = packet.getPort();
							logStatus("Received connection request from "+ username);
							
							if(gameStatus == GameStatus.RUNNING){
								send(address,port,"CONNECTION_REJECTED");
								continue;
							}
							
							send(address, port, "CONNECTION_GRANTED");
							netClients.put(username, new NetworkPlayer(username, address, port));
						}else if(data.startsWith("CONNECTED")){
							String username = data.split("\\s")[1];
							
							InetAddress address = packet.getAddress();
							int port = packet.getPort();
							//send to this user previously joined users
							for(String user : netClients.keySet()){
								send(address,port,"CONNECTED "+user);
							}
							//broadcast connection of new user
							broadcast("CONNECTED "+username);
							logStatus(username + " has connected!");
							if(netClients.size() == 3){
								broadcast("BATTLE_BEGIN");
								gameStatus = GameStatus.RUNNING;
							}
						}else if(data.startsWith("CHAT") || data.startsWith("BASE_STATE")){
							broadcast(data);
						}else if(data.startsWith("ATTACK")){
							
							String[] tokens = data.split("\\s");
							String attacker = tokens[1];
							String defender = tokens[2];
							
							//disable controls of attacker and defender
							sendToUser(attacker, "DISABLE");
							sendToUser(defender, "DISABLE");
							System.out.println(netClients.keySet());
							//compute TOTAL_HPs and TOTAL_DAMAGEs
							NetworkPlayer atk = netClients.get(attacker);
							NetworkPlayer def = netClients.get(defender);
							
							int def_base = def.getBaseHP();
							if(def_base == 0){
								sendToUser(defender, "PLEASE SET_UP_YOUR_BASE");
								sendToUser(attacker, "PLEASE DEFENDER_BASE_NOT_SET_UP");
								
							}else{
								int def_warrior = def.getWarriors();
								int def_archer = def.getArchers();
								
								int atk_warrior = atk.getWarriors();
								int atk_archer = atk.getArchers();
								
								
								sendToUser(defender, "PLEASE DEFENDING_AGAINST_"+attacker);
								sendToUser(attacker, "PLEASE ATTACKING_"+defender);
								
								int atk_total_hp = atk_warrior*100 + atk_archer*75;
								int atk_total_dmg = atk_warrior*25 + atk_archer*30;
								
								int def_total_hp = def_warrior*100 + def_archer*75;
								int def_total_dmg = def_warrior*25 + def_archer*30;
								
								int atk_weight = atk_total_hp - def_total_dmg;
								int def_weight = def_total_hp - atk_total_dmg;
								//send winner or loser notif
								try{
									if(atk_weight > def_weight){
										sendToUser(defender, "PLEASE LOST_AGAINST_"+attacker);
										sendToUser(attacker, "PLEASE WON_AGAINST_"+defender);
										atk.scored();
									}else if(atk_weight < def_weight){
										sendToUser(defender, "PLEASE WON_AGAINST_"+attacker);
										sendToUser(attacker, "PLEASE LOST_AGAINST_"+defender);
										def.scored();
									}else{
										sendToUser(defender, "PLEASE DRAW_AGAINST_"+attacker);
										sendToUser(attacker, "PLEASE DRAW_AGAINST_"+defender);
									}
									sendToUser(defender, "RESET STATS");
									sendToUser(attacker, "RESET STATS");
								}catch(EndGameException ege){
									broadcast(ege.getMessage()); //victory
								}
							}
							
							//enable controls
							sendToUser(attacker, "ENABLE");
							sendToUser(defender, "ENABLE");
							
						}else if(data.startsWith("TROOP")){
							String[] tokens = data.split("\\s");
							String username = tokens[1];
							String info = tokens[2];
							tokens = info.split(";");
							NetworkPlayer user = netClients.get(username);
							for(int i=0; i<tokens.length; i++){
								String[] s = tokens[i].split(":");
								int val = Integer.parseInt(s[1]);
								System.out.println("S1 is "+s[1]);
								switch(s[0]){
								case "WARRIORS":
									user.setWarriors(val);
									break;
								case "ARCHERS":
									user.setArchers(val);
									break;
								case "BASE":
									user.setBaseHP(val);
									System.out.println(username+ "base is "+val);
									break;
								}
							}
						}
						
						logReceivedPacket(packet.getSocketAddress().toString(), data);
					} catch (IOException e) {
						System.err.println("ERROR: "+e.getMessage());
						throw new PacketReceiveException("Server socket cannot receive packets!");
					}
				}
				
				if(serverSocket != null){
					serverSocket.close();
				}
			}
		});
	}
	
	public void startClientListener(){
		clientListener.start();
	}
	
	public void kill(){
		clientListener.interrupt();
		alive = false;
	}
	
	private void logStatus(String status){
		serverUI.logStatus(status);
	}
	
	private void logReceivedPacket(String source, String packet){
		serverUI.logReceivedPacket("["+source+"]: "+packet);
	}
	
	private void logSentPacket(String recipient, String packet){
		serverUI.logSentPacket("["+recipient+"]: "+packet);
	}
}
