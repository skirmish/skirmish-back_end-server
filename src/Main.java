import javax.swing.JOptionPane;

import exceptions.InvalidPortException;



public class Main {
	public static void main(String[] args) {
		int port = askPort();
		if(0==port)return;
		GameServer server = new GameServer(port);
	}
	
	private static int askPort(){
		int port = 0;
		while(port == 0){
			String portStr = JOptionPane.showInputDialog(null, "Port: ", "SKIRMISH | SERVER", JOptionPane.QUESTION_MESSAGE);
			if(portStr == null){
				return 0;
			}else{
				try{
					port = Validate.port(portStr);
				}catch(InvalidPortException ipe){
					Utils.popupError(ipe.getMessage());
					port = 0;
				}
			}
		}
		return port;
	}
}
