import java.net.InetAddress;

import exceptions.EndGameException;


public class NetworkPlayer {
	private String username;
	private int port;
	private InetAddress address;
	private int baseHP;
	private int warriors;
	private int archers;
	private int score;
	
	public NetworkPlayer(String username, InetAddress address, int port){
		this.username = username;
		this.address = address;
		this.port = port;
		baseHP = 0;
		warriors = 0;
		archers = 0;
		score = 0;
	}
	

	public int getBaseHP() {
		return baseHP;
	}


	public void setBaseHP(int baseHP) {
		this.baseHP = baseHP;
	}


	public int getWarriors() {
		return warriors;
	}


	public void setWarriors(int warriors) {
		this.warriors = warriors;
	}


	public int getArchers() {
		return archers;
	}


	public void setArchers(int archers) {
		this.archers = archers;
	}


	public String getUsername() {
		return username;
	}

	public int getPort() {
		return port;
	}

	public InetAddress getAddress() {
		return address;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NetworkPlayer other = (NetworkPlayer) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
	
	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append(username);
		builder.append("@");
		builder.append(address.getHostAddress());
		builder.append(":");
		builder.append(port);
		return builder.toString();
	}


	public void scored() {
		if(++score == 3){
			throw new EndGameException("VICTORY "+username);
		}
		
	}
}
