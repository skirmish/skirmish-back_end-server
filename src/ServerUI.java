import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class ServerUI {
	private final JFrame frame;
	private final LoggerPanel sentPackets;
	private final LoggerPanel receivedPackets;
	private final LoggerPanel status;
	
	public ServerUI(){
		frame = new JFrame("SKIRMISH | Server");
		sentPackets = new LoggerPanel("Packets sent");
		receivedPackets = new LoggerPanel("Packets received");
		status = new LoggerPanel("Status");
		init();
	}
	
	private void init(){
		setupCanvas();
		
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
	}

	public void display(){
		frame.setVisible(true);
	}
	
	private void setupCanvas() {
		JPanel canvas = new JPanel();
		
		status.setPreferredSize(new Dimension(640,220));
		canvas.add(status);
		canvas.add(receivedPackets);
		canvas.add(sentPackets);
		
		frame.setContentPane(canvas);
	}
	
	public void logStatus(String status){
		this.status.log(status);
	}
	
	public void logSentPacket(String packet){
		sentPackets.log(packet);
	}
	
	public void logReceivedPacket(String packet){
		receivedPackets.log(packet);
	}
}
