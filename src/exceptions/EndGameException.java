package exceptions;

public class EndGameException extends RuntimeException {

	private static final long serialVersionUID = -9069895647695092368L;

	public EndGameException() {
		
	}

	public EndGameException(String message) {
		super(message);
		
	}

	public EndGameException(Throwable cause) {
		super(cause);
		
	}

	public EndGameException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public EndGameException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

}
