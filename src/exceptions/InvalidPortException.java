package exceptions;

public class InvalidPortException extends RuntimeException {

	private static final long serialVersionUID = -5867717890880443318L;

	public InvalidPortException() {

	}

	public InvalidPortException(String message) {
		super(message);

	}

	public InvalidPortException(Throwable cause) {
		super(cause);

	}

	public InvalidPortException(String message, Throwable cause) {
		super(message, cause);

	}

	public InvalidPortException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
