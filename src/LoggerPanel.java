import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;


public class LoggerPanel extends JPanel {
	private static final long serialVersionUID = 1016842340895232049L;
	private final JTextArea logger;
	
	public LoggerPanel(String title){
		super();
		setLayout(new BorderLayout());
		setBackground(Color.GRAY);
		setBorder(BorderFactory.createTitledBorder(title));
		logger = new JTextArea(20,35);
		
		logger.setEditable(false);
		logger.setLineWrap(true);
		
		setupTextArea();
		
	}

	private void setupTextArea() {
		/*chat logs*/
		DefaultCaret caret = (DefaultCaret)logger.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		add(new JScrollPane(logger), BorderLayout.NORTH);
	}
	
	public void clear(){
		logger.setText("");
	}
	
	public void log(String message){
		logger.append(message+"\n");
	}
}
